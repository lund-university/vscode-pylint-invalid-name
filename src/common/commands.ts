import * as vscode from 'vscode';
import * as fs from 'fs';
import * as path from 'path';
import { exec } from 'child_process';

export async function disableNamingStandard(documentUri: any): Promise<void> {
    const uri = vscode.Uri.parse(documentUri.path);
    const workspaceFolder = vscode.workspace.getWorkspaceFolder(uri);
    const workspacePath = workspaceFolder ? workspaceFolder.uri.fsPath : undefined;
    const pylintrcPath = workspacePath ? path.join(workspacePath, 'pylintrc') : undefined;

    if (pylintrcPath !== undefined) {
        try {
            if (!fs.existsSync(pylintrcPath)) {
                // If the pylintrc file doesn't exist, create a new one using the pylint command
                const generateCmd = `pylint --generate-rcfile > "${pylintrcPath}"`;
                exec(generateCmd, (error, stdout, stderr) => {
                    if (error) {
                        vscode.window.showErrorMessage(`Error generating pylintrc: ${error.message}`);
                        return;
                    }

                    if (stderr) {
                        vscode.window.showErrorMessage(`Error generating pylintrc: ${stderr}`);
                        return;
                    }

                    // Read the generated pylintrc file and modify it to disable the 'invalid-name' rule
                    modifyPylintrcFile(pylintrcPath);
                });
            } else {
                // If the pylintrc file exists, modify it to disable the 'invalid-name' rule
                modifyPylintrcFile(pylintrcPath);
            }
        } catch (error: any) {
            if (error.code === 'EROFS') {
                vscode.window.showErrorMessage(`Cannot update pylintrc: ${pylintrcPath} is a read-only file system.`);
            } else {
                vscode.window.showErrorMessage(`Error updating pylintrc: ${error.message}`);
            }
        }
    } else {
        vscode.window.showErrorMessage('Cannot generate or modify pylintrc: no workspace found.');
    }
}


export async function addToGoodNames(documentUri: any, codeName: string): Promise<void> {
    const uri = vscode.Uri.parse(documentUri.path);
    const workspaceFolder = vscode.workspace.getWorkspaceFolder(uri);
    const workspacePath = workspaceFolder ? workspaceFolder.uri.fsPath : undefined;
    const pylintrcPath = workspacePath ? path.join(workspacePath, 'pylintrc') : undefined;

    if (pylintrcPath !== undefined) {
        try {
            if (!fs.existsSync(pylintrcPath)) {
                // If the pylintrc file doesn't exist, create a new one using the pylint command
                const generateCmd = `pylint --generate-rcfile > "${pylintrcPath}"`;
                exec(generateCmd, (error, stdout, stderr) => {
                    if (error) {
                        vscode.window.showErrorMessage(`Error generating pylintrc: ${error.message}`);
                        return;
                    }

                    if (stderr) {
                        vscode.window.showErrorMessage(`Error generating pylintrc: ${stderr}`);
                        return;
                    }

                    // Read the generated pylintrc file and modify it to add the code name to good-names
                    modifyPylintrcFileToAddName(pylintrcPath, codeName);
                });
            } else {
                // If the pylintrc file exists, modify it to add the code name to good-names
                modifyPylintrcFileToAddName(pylintrcPath, codeName);
            }
        } catch (error: any) {
            if (error.code === 'EROFS') {
                vscode.window.showErrorMessage(`Cannot update pylintrc: ${pylintrcPath} is a read-only file system.`);
            } else {
                vscode.window.showErrorMessage(`Error updating pylintrc: ${error.message}`);
            }
        }
    } else {
        vscode.window.showErrorMessage('Cannot generate or modify pylintrc: no workspace found.');
    }
}

function modifyPylintrcFileToAddName(pylintrcPath: string, codeName: string): void {
    const currentContent = fs.readFileSync(pylintrcPath, 'utf8');
    const lines = currentContent.split('\n');
    let namingIndex = -1;
    let goodNamesLineIndex = -1;

    // Find the [BASIC] section and the good-names= line
    for (let i = 0; i < lines.length; i++) {
        if (lines[i].trim() === '[BASIC]') {
            namingIndex = i;
        }
        if (lines[i].startsWith('good-names=')) {
            goodNamesLineIndex = i;
        }
    }

    if (namingIndex >= 0) {
        if (goodNamesLineIndex >= 0) {
            // If the good-names= line exists, check if the codeName is already present
            if (!lines[goodNamesLineIndex].includes(codeName)) {
                // If the codeName is not present, prepend it to the list of good-names
                lines[goodNamesLineIndex] = lines[goodNamesLineIndex].replace('good-names=', `good-names=${codeName},`);
            } else {
                vscode.window.showInformationMessage(`The code name "${codeName}" is already in the list of good-names.`);
            }
        } else {
            // If the good-names= line doesn't exist, add a new one under the [BASIC] section
            lines.splice(namingIndex + 1, 0, `good-names=${codeName}`);
        }
        fs.writeFileSync(pylintrcPath, lines.join('\n'));
        vscode.window.showInformationMessage(`Successfully updated the pylintrc file to add "${codeName}" to the list of good-names.`);
    } else {
        vscode.window.showErrorMessage('The [BASIC] section is not found in the pylintrc file.');
    }
}


function modifyPylintrcFile(pylintrcPath: string): void {
    const currentContent = fs.readFileSync(pylintrcPath, 'utf8');
    const lines = currentContent.split('\n');
    let messagesControlIndex = -1;
    let disableLineIndex = -1;

    // Find the [MESSAGES CONTROL] section and the disable= line
    for (let i = 0; i < lines.length; i++) {
        if (lines[i].trim() === '[MESSAGES CONTROL]') {
            messagesControlIndex = i;
        }
        if (lines[i].startsWith('disable=')) {
            disableLineIndex = i;
        }
    }

    if (messagesControlIndex >= 0) {
        if (disableLineIndex >= 0) {
            // If the disable= line exists, check if 'invalid-name' is already present
            if (!lines[disableLineIndex].includes('invalid-name')) {
                // If 'invalid-name' is not present, prepend it to the list of disabled rules
                lines[disableLineIndex] = lines[disableLineIndex].replace('disable=', 'disable=invalid-name,');
            } else {
                vscode.window.showInformationMessage('The naming standard violation is already disabled in the pylintrc file.');
            }
        } else {
            // If the disable= line doesn't exist, add a new one under the [MESSAGES CONTROL] section
            lines.splice(messagesControlIndex + 1, 0, 'disable=invalid-name');
        }
        fs.writeFileSync(pylintrcPath, lines.join('\n'));
        vscode.window.showInformationMessage('Successfully updated the pylintrc file to disable the naming standard violation.');
    } else {
        vscode.window.showErrorMessage('The [MESSAGES CONTROL] section is not found in the pylintrc file.');
    }
}
