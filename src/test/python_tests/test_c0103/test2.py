"""
This exercise should be done in 5 minutes. There are 2 main objectives:

1. Eliminate every linted, underlined and highlighted code.
2. Every naming term should be in lower letters (snake case).

You can refresh the warnings by pressing command + S saving the file.

Convention types:
- snake_case: Lowercase letters and underscores to separate words. Example: user_name, my_module.
- PascalCase: Uppercase letters for the first letter of each word, with no underscores.
       Example: CalculateTotalPrice.
- UPPER_CASE: Uses uppercase letters and underscores to separate words. Example: TAX_RATE.

"""
import datetime

name = "John"
age = 25
print("My name is", name, "and I am", age, "years old.")

def convert_to_seconds(hours):
    """Convert minutes to seconds"""
    seconds = minutes * 60
    return seconds

import random

class animal:
    """Animal class"""
    pass

def calculate_area_of_circle(radius):
    """Calculate area of circle"""
    area = math.pi * radius ** 2
    return area

sentence = "The quick brown fox jumps over the lazy dog."
print("The length of the sentence is:", len(sentence), "characters.")
