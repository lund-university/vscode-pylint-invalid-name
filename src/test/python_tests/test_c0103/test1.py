"""
This exercise should be done in 5 minutes. There are 2 main objectives:

1. Eliminate every linted, underlined and highlighted code.
2. Every naming term should be in cappital letters (UPPER_CASE).

You can refresh the warnings by pressing command + S saving the file.

Convention types:
- snake_case: Lowercase letters and underscores to separate words. Example: user_name, my_module.
- PascalCase: Uppercase letters for the first letter of each word, with no underscores.
       Example: CalculateTotalPrice.
- UPPER_CASE: Uses uppercase letters and underscores to separate words. Example: TAX_RATE.

"""
a = 32
print(a+b)

def create_directory(directory_path):
    """Create directory"""
    DIRECT = os.makedirs(directory_path)
    return DIRECT

import math

class employee:
    """Employee class"""
    pass

def calculate_product_price(product_name, product_type, quantity, price_per_unit, tax_rate):
  total_price = quantity * price_per_unit
  tAx = total_price * tax_rate
  Final_price = total_price + tAx
  return Final_price

text = "Fusce commodo, tellus nec varius bibendum, nisi est fringilla justo, et convallis augue elit in dolor."
